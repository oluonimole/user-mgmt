@extends('layouts.app')


@section('title')
    <h1>Users</h1>
@endsection


@section('content')

    <table style="border: 1px solid black;   border-collapse: none; width: 100% ;">

        <tr>    
            <th style="padding: 10px">{{ 'ID' }}</th>
            <th style="padding: 10px">{{ 'NAME' }}</th>
            <th style="padding: 10px">{{ 'EMAIL' }}</th>
            <th style="padding: 10px">{{ 'DOB' }}</th>
            <th style="padding: 10px">{{ 'PHONE NUMBER' }}</th>
        </tr>


        @foreach($items as $item)
            <tr>    
                <td style="padding: 10px">{{ $item->id }}</td>
                <td style="padding: 10px">{{ $item->name }}</td>
                <td style="padding: 10px">{{ $item->email }}</td>
                <td style="padding: 10px">{{ $item->dateOfBirth }}</td>
                <td style="padding: 10px">{{ $item->phoneNumber }}</td>
            </tr>
        @endforeach

    </table>

@endsection



