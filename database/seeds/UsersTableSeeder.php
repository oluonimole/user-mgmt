<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        
        DB::table('users')->insert([

        'dateOfBirth' => '09 jun 2000',
        'name' => 'User3',
        'phoneNumber' => '07545185600',
        'email' => 'user3@email.com',
        'password' => bcrypt('password'),

        ]);
    
        DB::table('users')->insert([

        'dateOfBirth' => '05 apr 1918',
        'name' => 'User4',
        'phoneNumber' => '07688907643',
        'email' => 'user4@email.com',
        'password' => bcrypt('password'),

        ]);
    }
}
